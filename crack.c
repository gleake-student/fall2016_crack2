#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "md5.h"

//const int PASS_LEN=20;        // Maximum any password can be
//const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *hashTry = md5(guess, (strlen(guess)));
    // Compare the two hashes
    if (strcmp(hashTry, hash) == 0) {
        return 1;
    } else {
        return 0;
    }
    // Free any malloc'd memory
    free(hashTry);
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    struct stat st;
    stat(filename, &st);
    int file_len = st.st_size;
    
    //allocate memory for a buffer
    char *schrodinger =  malloc( (file_len + 1) * sizeof(char));
    //set last entry as a null
    schrodinger[file_len] = '\0';
    //Open files for reading
    FILE *h = fopen(filename, "r");
    if (!h){//check if file can be opened
        printf("Can't open %s for reading\n", filename);
    }
    fread(schrodinger, 1, file_len, h);
    
    //Close files
    fclose(h);
    
    //create counter to count number of lines in file
    int lineCount = 0;
    for (int i = 0; i < file_len; i++){
        if (schrodinger[i] == '\n'){
            schrodinger[i] = '\0';
            lineCount++;
        }
    }
    printf("there are %d lines in the file", lineCount);//prototyping count of lines. delete when working
    
    char ** strings = malloc((lineCount) * sizeof(char *));
    int stringCount = 0;
    for (int i = 0; i < file_len; i += strlen(schrodinger + i) + 1){
        strings[stringCount] = schrodinger + i;
        
        if(stringCount < lineCount) stringCount++; 
    }
    
    strings[stringCount] = NULL; //end stringCount with null
    
    int i = 0;//check to see if end of strings is reached and stored
    while(strings[i] != NULL){
        i++;
    }
    return strings; //returns strings to main
    
    free(schrodinger);
    free(strings);
    
    
    
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
        // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);
    puts("Hashes stored");
    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);
    puts("dictionary stored");
    
    int hashCount = 0, dictCount = 0;
    
    // For each hash, try every entry in the dictionary.
    while (hashes[hashCount] != NULL){
        while(dict[dictCount] != NULL ){
          if (tryguess(hashes[hashCount], dict[dictCount]) == 1){
              printf("%s matches %s \n", hashes[hashCount], dict[dictCount]);
              dictCount = 0;
              break;
          }
          dictCount++;  
        }
       hashCount++;
    }
    // Print the matching dictionary entry.
    // Need two nested loops.
}
